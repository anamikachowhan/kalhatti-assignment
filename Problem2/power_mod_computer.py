def power_function(x, n):
    if (n==0):
        return 1
    if (n==1):
        return x
    return x*power_function(x, n-1)

def find_power_mod(x, n , d):
    power = power_function(x,n)
    if(d == 0):
        return "Undefined"
    if(power<= 0):
        return str(power%d) + " Or " +str((power*-1)%d*(-1))
    return power%d


    