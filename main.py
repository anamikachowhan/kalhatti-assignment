from Problem1 import path_finder as path_finder
from Problem1 import tree_node as tree_node
from Problem2 import power_mod_computer as power_mod_computer

def test_problem1():
    '''
    node = tree_node.TreeNode(-4)
    node.left = tree_node.TreeNode(5)
    node.left.left = tree_node.TreeNode(-17)
    node.left.right = tree_node.TreeNode(8)
    node.right = tree_node.TreeNode(-12)
    node.right.left = tree_node.TreeNode(9)
    node.right.right = tree_node.TreeNode(10)
    '''

    node = tree_node.TreeNode(5)
    node.left = tree_node.TreeNode(4)
    node.left.left = tree_node.TreeNode(11)
    node.left.left.left = tree_node.TreeNode(7)
    node.left.left.right = tree_node.TreeNode(2)
    
    node.right = tree_node.TreeNode(8)
    node.right.left = tree_node.TreeNode(13)
    node.right.right = tree_node.TreeNode(4)
    node.right.right.left = tree_node.TreeNode(5)
    node.right.right.right = tree_node.TreeNode(1)

    return path_finder.find_path_equal_to_sum(22,0,node,'',[])
   

def test_problem2():
    return power_mod_computer.find_power_mod(-2, 3 , 3)

print('Outputs for Problem1 and problem2 for inputs on given site are as follows')
print(str(test_problem1()))
print(str(test_problem2()))
