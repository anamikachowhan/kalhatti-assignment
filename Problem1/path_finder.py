import tree_node as tree_node
#paths = []
def find_path_equal_to_sum(sum, traversed_sum,root, path, paths):    
    if root == None:
        return False 
    traversed_sum = traversed_sum + root.data
    #print(root.data,'  ') 
    path = path + str(root.data) +', '
    if traversed_sum == sum:
        paths.append(path)
        traversed_sum = 0
        path = ''
        #print('path is ', str(paths) )
        return True
    find_path_equal_to_sum(sum,traversed_sum, root.left, path, paths)
    find_path_equal_to_sum(sum,traversed_sum, root.right, path, paths)
    return paths
    




